- Import JSON
- Search
- Cache frequently accessed elements
- Initialise with cache hint list

# Examples

```json
{ id:"126.1.IBM",
 name:"International Business Machines",
 color:"#AA008A",
 securityToken:"F0000008V0]",
 currency:"GBP"
}
```

```json
{
  "firstName": "John",
  "lastName": "Smith",
  "isAlive": true,
  "age": 27,
  "address": {
    "streetAddress": "21 2nd Street",
    "city": "New York",
    "state": "NY",
    "postalCode": "10021-3100"
  },
  "phoneNumbers": [
    {
      "type": "home",
      "number": "212 555-1234"
    },
    {
      "type": "office",
      "number": "646 555-4567"
    }
  ],
  "children": [],
  "spouse": null
}
```

# Explore
- Caching strategies

# References
- https://en.wikipedia.org/wiki/JSON

